from django.db import models

from authentication.models import User
from todo.choices import CHOICE_STATE


class Task(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    description = models.TextField()
    state = models.CharField(choices=CHOICE_STATE, max_length=15, null=False, blank=False, default='incomplete')
    create_date = models.DateTimeField(null=False, blank=False, auto_now_add=True, verbose_name='Creation date')

    class Meta:
        verbose_name = 'Tarea'
        verbose_name_plural = 'Tareas'

    def get_full_title(self):
        """
        Returns the first_name and the last_name
        """
        return self.title

    def get_short_title(self):
        """
        Returns the short name for the user.
        """
        return self.title[0:20]

    @classmethod
    def get_by_id(cls, uid):
        return Task.objects.get(id=uid)

    def __str__(self):
        return self.title

