from django.shortcuts import render, get_object_or_404
from rest_framework import viewsets, status, generics
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response

from todo.filters import TaskFilter
from todo.models import Task
from todo.serializers import TaskSerializer


class TaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer

    def retrieve(self, request, *args, **kwargs):
        queryset = Task.objects.filter(user=request.user)
        task = get_object_or_404(queryset, id=kwargs['pk'])
        serializer = TaskSerializer(task, context={'request': request})
        data = serializer.data
        return Response(data)

    def list(self, request, *args, **kwargs):
        query = Task.objects.filter(user=request.user)
        filters = TaskFilter(request.GET, queryset=query)
        queryset = filters.qs

        page = request.GET.get('page')
        try:
            page = self.paginate_queryset(queryset)
        except Exception as e:
            page = []
            data = page
            return Response({
                "status": status.HTTP_404_NOT_FOUND,
                "message": 'No more record.',
                "data" : data
            })

        if page is not None:
            serializer = self.get_serializer(page, many=True)
            data = serializer.data
            return self.get_paginated_response(data)

        return Response(status=status.HTTP_404_NOT_FOUND)

    def create(self, request, *args, **kwargs):
        data = request.data
        res = {}
        try:
            if data['title'] and data['description'] and data['state']:
                task = Task(user=request.user, title=data['title'], description=data['description'], state=data['state'])
                task.save()
                serializer_context = {
                    'request': request,
                }
                serializer = TaskSerializer(task, many=False, context=serializer_context)
                if serializer:
                    return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                if not data['title']:
                    res['title']: 'El título es requerido'

                if not data['description']:
                    res['description']: 'La descripción es requerida'

                if not data['state']:
                    res['state']: 'El estado es requerido'

        except KeyError as e:
            res[str(e)] = 'Este campo es requerido'

        return Response(res, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, *args, **kwargs):
        task = Task.objects.filter(id=kwargs['pk'], user=request.user).first()
        if task:
            data = request.data
            if data['title'] and task.title != data['title']:
                task.title = data['title']
            if data['description'] and task.description != data['description']:
                task.description = data['description']
            if data['state'] and task.state != data['state']:
                task.state = data['state']
            task.save()
            serializer = TaskSerializer(task, context={'request': request})
            data = {
                'message': "Tarea actualizada",
                'task': serializer.data
            }
            return Response(data, status=status.HTTP_201_CREATED)
        return Response(status=status.HTTP_404_NOT_FOUND)

    def destroy(self, request, *args, **kwargs):
        task = Task.objects.filter(id=kwargs['pk'], user=request.user).first()
        if task:
            task.delete()
            serializer = TaskSerializer(task, context={'request': request})
            data = {
                'message': "Tarea eliminada",
                'task': serializer.data
            }
            return Response(data, status=status.HTTP_201_CREATED)
        return Response(status=status.HTTP_404_NOT_FOUND)
