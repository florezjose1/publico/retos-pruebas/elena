from rest_framework import serializers

from todo.models import Task


class TaskSerializer(serializers.HyperlinkedModelSerializer):
    create_date = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S")

    class Meta:
        model = Task
        fields = ['id', 'title', 'description', 'state', 'create_date']
