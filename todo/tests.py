from django.test import TestCase

from authentication.models import User
from todo.models import Task


class UserModelTest(TestCase):

    def setUp(self):
        self.test_user = User(email="florezjoserodolfo@gmail.com", first_name='Jose', last_name='Florez')
        self.test_user.save()
        self.test_task = Task(title="Tarea 1", user=self.test_user, description="Descripción tarea 1",
                              state='incomplete')
        self.test_task.save()

    def test_user_to_string_email(self):
        self.assertEquals(str(self.test_task), "Tarea 1")

    def test_state_task(self):
        task = Task.get_by_id(1)
        self.assertNotEqual(task.state, 'completed')

    def test_get_by_id(self):
        self.assertEquals(Task.get_by_id(1), self.test_task)

    def tearDown(self):
        self.test_task.delete()
