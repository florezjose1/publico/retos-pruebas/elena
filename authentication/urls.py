from django.urls import path
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
from .views import CustomUserCreate


urlpatterns = [
    path('user/create/', CustomUserCreate.as_view(), name="create_user"),
    path('user/login/', TokenObtainPairView.as_view(), name='login_user'),
    path('user/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]
