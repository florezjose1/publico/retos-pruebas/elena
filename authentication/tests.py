from django.test import TestCase

from authentication.models import User


class UserModelTest(TestCase):

    def setUp(self):
        self.test_user = User(email="florezjoserodolfo@gmail.com", first_name='Jose', last_name='Florez')
        self.test_user.save()

    def test_user_to_string_email(self):
        self.assertEquals(str(self.test_user), "florezjoserodolfo@gmail.com")

    def test_get_by_id(self):
        self.assertEquals(User.get_by_id(1), self.test_user)

    def tearDown(self):
        self.test_user.delete()
