from django.contrib.auth.base_user import AbstractBaseUser
from django.db import models
from django.contrib.auth.models import AbstractUser, PermissionsMixin, UserManager


class User(AbstractUser, PermissionsMixin):
    email = models.EmailField(verbose_name='Correo electrónico', unique=True)
    first_name = models.CharField(verbose_name='Nombre(s)', max_length=30, blank=True)
    last_name = models.CharField(verbose_name='Apellido(s)', max_length=30, blank=True)
    date_joined = models.DateTimeField(verbose_name='Fecha de registro', auto_now_add=True)
    is_active = models.BooleanField(verbose_name='Activo', default=True)
    is_superuser = models.BooleanField(verbose_name='Super usuario', default=False)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username', 'password']
    objects = UserManager()

    class Meta:
        verbose_name = 'Usuario'
        verbose_name_plural = 'Usuarios'

    def get_full_name(self):
        """
        Returns the first_name and the last_name
        """
        return self.first_name + " " + self.last_name

    def get_short_name(self):
        """
        Returns the short name for the user.
        """
        return self.first_name

    @classmethod
    def get_by_id(cls, uid):
        return User.objects.get(id=uid)

    def __unicode__(self):
        return self.email
