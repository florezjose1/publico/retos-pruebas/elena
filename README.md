# Elena - Prueba Backend

Versión Python: `^3.5`

Versión django: `3.1.7`

#### Configuración del proyecto (Linux/Windows)

* Clone repositorio

```
$ git clone
$ cd elena
```

* Cree virtulenv

```
$ virtualenv -p python3 venv | virtualenv env
$ source venv/bin/activated | ./env/Scripts/activate.bat
```

* Instale requerimientos

```
pip3 install -r requeriments.txt
pip install -r requeriments.txt
```

* Ejecute migraciones:

```
$ python manage.py migrate
```

* Ejecutar

```
python3 manage.py runserve
python manage.py runserve
```

* Test

```
python3 manage.py test
python manage.py test
```

## API

### Autenticación

* #### Regristro (POST) `/api/user/create/`

```javascript
fetch('http://127.0.0.1:8000/api/user/create/', {
    method: 'POST',
    body: JSON.stringify({email: 'florezjose@gmail.com', username: 'florezjose', password: '1234jose'}),
    headers: {
        'Content-Type': 'application/json'
    }
}).then(response => {
    response.json().then(res => {
        console.log(res)
    })
}).catch((e) => {
    console.error(e)
})
```

* #### Inicio de Sesión (POST) `/api/user/login/`

```javascript
fetch('http://127.0.0.1:8000/api/user/login/', {
    method: 'POST',
    body: JSON.stringify({email: 'florezjose@gmail.com', password: '1234jose'}),
    headers: {
        'Content-Type': 'application/json'
    }
}).then(response => {
    response.json().then(res => {
        console.log(res)
    })
}).catch((e) => {
    console.error(e)
})
```

* #### Refresh (POST) `/api/user/refresh/`

```javascript
fetch('http://127.0.0.1:8000/api/user/refresh/', {
    method: 'POST',
    body: JSON.stringify({refresh: "token"}),
    headers: {
        'Content-Type': 'application/json'
    }
}).then(response => {
    response.json().then(res => {
        console.log(res)
    })
}).catch((e) => {
    console.error(e)
})
```

### Tareas

* #### Listar (GET) `/api/task/`

```javascript
fetch('http://127.0.0.1:8000/api/task/', {
    method: 'GET',
    headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`,
    }
}).then(response => {
    response.json().then(res => {
        console.log(res)
    })
}).catch((e) => {
    console.error(e)
})
```

* #### Crear (POST) `/api/task/`

```javascript
fetch('http://127.0.0.1:8000/api/task/', {
    method: 'POST',
    body: JSON.stringify({title: 'Post 1', description: 'Description 1', state: 'incomplete'}),
    headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`,
    }
}).then(response => {
    response.json().then(res => {
        console.log(res)
    })
}).catch((e) => {
    console.error(e)
})
```

* #### Actualizar (PUT) `/api/task/{ID}/`

```javascript
fetch('http://127.0.0.1:8000/api/task/1/', {
    method: 'PUT',
    body: JSON.stringify({title: 'Post 1', description: 'Description 1', state: 'incomplete'}),
    headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`,
    }
}).then(response => {
    response.json().then(res => {
        console.log(res)
    })
}).catch((e) => {
    console.error(e)
})
```

* #### Eliminar (DELETE) `/api/task/{ID}/`

```javascript
fetch('http://127.0.0.1:8000/api/task/1/', {
    method: 'DELETE',
    headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`,
    }
}).then(response => {
    response.json().then(res => {
        console.log(res)
    })
}).catch((e) => {
    console.error(e)
})
```

* #### Filtrar (GET) `/api/task/?options`

options: { title=?, decription=? }

```javascript
fetch('http://127.0.0.1:8000/api/task/?description=des', {
    method: 'GET',
    headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`,
    }
}).then(response => {
    response.json().then(res => {
        console.log(res)
    })
}).catch((e) => {
    console.error(e)
})
```

Hecho con ♥ por [Jose Florez](www.joseflorez.co)
